#include "stdafx.h"

#define KPH_DEVICE_SHORT_NAME   L"KProcessHacker2"
#define KPH_DEVICE_NAME			(L"\\Device\\" KPH_DEVICE_SHORT_NAME)

#define KPH_DEVICE_TYPE			0x9999
#define KPH_CTL_CODE(x)			CTL_CODE(KPH_DEVICE_TYPE, 0x800 + x, METHOD_NEITHER, FILE_ANY_ACCESS)
#define KPH_READMEMUNSAFE_CODE	KPH_CTL_CODE(58)

HANDLE kphHandle;

VOID		(NTAPI * RtlInitUnicodeString)(PUNICODE_STRING64 DestinationString, PCWSTR SourceString);
NTSTATUS	(NTAPI * NtOpenFile)(PHANDLE FileHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes, PIO_STATUS_BLOCK IoStatusBlock, ULONG ShareAccess, ULONG OpenOptions);

bool KPHReadMemory(HANDLE hProcess, LPCVOID lpBaseAddress, LPVOID lpBuffer, SIZE_T nSize, SIZE_T *lpNumberOfBytesRead)
{
	struct
	{
		HANDLE	ProcessHandle;
		PVOID	BaseAddress;
		PVOID	Buffer;
		SIZE_T	BufferSize;
		PSIZE_T NumberOfBytesRead;
	} data;

	data.ProcessHandle		= hProcess;
	data.BaseAddress		= (PVOID)lpBaseAddress;
	data.Buffer				= lpBuffer;
	data.BufferSize			= nSize;
	data.NumberOfBytesRead	= lpNumberOfBytesRead;

	DWORD bytesRet;
	if (DeviceIoControl(kphHandle, KPH_READMEMUNSAFE_CODE, &data, sizeof(data), &data, sizeof(data), &bytesRet, NULL))
		return true;

	return false;
}

int main(int argc, char *argv[])
{
	HMODULE hNtdll						= GetModuleHandleA("ntdll.dll");
	*(FARPROC *)&RtlInitUnicodeString	= GetProcAddress(hNtdll, "RtlInitUnicodeString");
	*(FARPROC *)&NtOpenFile				= GetProcAddress(hNtdll, "NtOpenFile");

	UNICODE_STRING64 objectName;
	RtlInitUnicodeString(&objectName, KPH_DEVICE_NAME);

	OBJECT_ATTRIBUTES objectAttributes;
	InitializeObjectAttributes(
		&objectAttributes,
		&objectName,
		OBJ_CASE_INSENSITIVE,
		NULL,
		NULL
		);

	_IO_STATUS_BLOCK isb;
	long status = NtOpenFile(
		&kphHandle,
		FILE_GENERIC_READ | FILE_GENERIC_WRITE,
		&objectAttributes,
		&isb,
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		FILE_NON_DIRECTORY_FILE
		);

	if (status != 0)
	{
		printf("Unable to open a handle to %ws\n", objectName.Buffer);
		getchar();

		return 1;
	}

	// KPHReadMemory(GetCurrentProcess(), 0xfffff........., buffer, size, nullptr);

	printf("Successfully opened handle to process hacker's driver, can read memory.\n");
	getchar();

	return 0;
}